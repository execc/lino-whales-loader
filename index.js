const rlino = require('lino-js');
const dateFormat = require('dateformat');
const mysql = require('mysql');
const cron = require('node-cron');

const users = require('./userbase');

// Set up node connection
//
const NODE = 'http://163.172.139.34:26657';
const TESTNET_NODE = 'https://fullnode.linovalidator.io';
const CHAIN_ID = 'lino-testnet';

const lino = new rlino.LINO({
    nodeUrl: TESTNET_NODE,
    chainId: CHAIN_ID
});

// Set up mysql connnection pool
//
const DB_HOST = '163.172.139.34';
const DB_USER = 'root';
const DB_PASS = '123456';
const DB_NAME = 'lino';

const pool = mysql.createPool({
    connectionLimit: 10,
    host: DB_HOST,
    user: DB_USER,
    password: DB_PASS,
    database: DB_NAME
});

// Set up crontab
//
const SCHEDULE = '0 17 * * *';

// Test users aybuse, indy8phish, mariandreapf
//
// let users = ['mariandreapf', 'indy8phish', 'aybuse'];

async function readUserData(username, ifx, date) {
    const ident = username + ' (' + ifx + ')'
    console.log('[Info] Start processing user: ' + ident);

    // Get funds information on each account (for estimating account value)
    // This querty will return amounts of LINO and LP (locking points)
    //
    console.log('[Info] [getAccountBank] for user: ' + ident);
    let accountBank = await lino.query.getAccountBank(username);

    // Get funds raised by account
    //
    console.log('[Info] [getReward] for user: ' + ident);
    let reward = await lino.query.getReward(username);

    // Get user posts. This querty will returns amount of user posts
    //
    console.log('[Info] [getAllPosts] for user: ' + ident);
    let posts = await lino.query.getAllPosts(username);
    let postsCount = posts.length;

    // Get user follower meta. This query will return amount of user followers
    //
    console.log('[Info] [getAllFollowerMeta] for user: ' + ident);
    let followers = await lino.query.getAllFollowerMeta(username);
    let followersCount = followers.length;

    // Get user following meta. This querty will return amount of users this user follows
    //
    console.log('[Info] [getAllFollowingMeta] for user: ' + ident);
    let following = await lino.query.getAllFollowingMeta(username);
    let followingCount = following.length;

    const row = {
        date: date,
        username: username,
        lino: accountBank.saving.amount,
        lp: accountBank.frozen_money_list ? accountBank.frozen_money_list.reduce((acc, val) => val.amount.amount, 0) : 0,
        posts: postsCount,
        followers: followersCount,
        following: followingCount,
        original_income: reward.original_income.amount,
        friction_income: reward.friction_income.amount,
        inflation_income: reward.inflation_income.amount,
    };
    console.log('[Info] End processing user: ' + ident);
   
    console.log('[Info] Insert into mysql: ' + ident);
    pool.query('INSERT INTO USER_STATISTICS SET ?', row, (error) => {
        if (error) {
            console.log('[Debug] Failed to insert row for user: ' + username + ' is ' + JSON.stringify(row));
            throw error;
        }
    });
};

// Schedule execution
//
console.log('[Info] Scheduling execution using pattern: ' + SCHEDULE);
console.log('[Info] Users to analyze: ' + users.length);

cron.schedule(SCHEDULE, () => {
    // This script will run on cron evey day. Set up current date to write to databse
    //
    const date = dateFormat(new Date(), 'yyyy-mm-dd');
    users.forEach((user, index) => {
        readUserData(user, (index + 1) + '/' + len, date);
    });
});